#!/usr/bin/env python3

import sys
import logging
import json
import yaml
import paho.mqtt.client
# from influxdb import InfluxDBClient
#import toinflux
import argparse
import os

__version__ = '@@VERSION@@'

LOG_FORMAT = '%(asctime)s %(levelname)s: %(message)s'

mqtt_rules = {
    "temperature": ["temperature"],
    "temperature-integrated": ["temperature-integrated"],
    "relative-humidity": ["relative-humidity"],
    "concentration": ["concentration"],
    "illuminance": ["illuminance"],
    "pressure": ["pressure"],
    "altitude": ["altitude"],
}

config = {
    'mqtt': {
        'host': 'localhost',
        'port': 1883,
        'topic': 'node',
        'username': '',
        'password': '',
        'cafile': None,
        'certfile': None,
        'keyfile': None,
    },
    'influx': {
        'config': '/etc/bigclown/influx.ini',
        'prefix': '',
    },
    'leds': [{
        'path': '',
        'measurement': '',
        'thresholds': [{
            'val': 0,
            'rgb': '255 255 255',
            }],
        'maxcolor': '255 255 255',
        }],
}

def turris_led(measurement, value, userdata):
    leds = userdata["leds"]
    for led in leds:
        if led["measurement"] == measurement:
            print_mqtt(measurement, value, userdata)
            color=led["maxcolor"]
            for threshold in led["thresholds"]:
                if value < threshold["val"]:
                    color=threshold["rgb"]
                    break
            print("color: ", color)
            os.system("echo '"+color+"' > "+led["path"])

def print_mqtt(measurement, value, userdata):
    print(measurement, "= ", end="")
    print(value)

def mqtt_on_connect(client, userdata, flags, rc):
    logging.info('Connected to MQTT broker with (code %s)', rc)
    client.subscribe(userdata['base_topic'] + '+/+/+/+')


def mqtt_on_message(client, userdata, msg):
    try:
        payload = json.loads(msg.payload.decode('utf-8'))
    except Exception as e:
        return

    topic = msg.topic.split('/')
    # print(topic, payload)
    measurement = '/'.join(topic[1:5])

    #influx = userdata["influx"]

    if measurement == 'temperature' and dev == 'thermometer/0:1':
        measurement = 'temperature-integrated'

    if isinstance(payload, str):
        return

    if isinstance(payload, dict):
        return

    #print(response)
    turris_led(measurement, payload, userdata)
    #influx.parse_result(response, tags)


def main():
    argp = argparse.ArgumentParser(description='BigClown gateway between USB serial port and MQTT broker')
    argp.add_argument('-c', '--config', help='path to configuration file (YAML format)')
    argp.add_argument('-H', '--mqtt-host', help='MQTT host to connect to (default is localhost)')
    argp.add_argument('-P', '--mqtt-port', help='MQTT port to connect to (default is 1883)')
    argp.add_argument('-t', '--mqtt-topic', help='base MQTT topic (default is node)')
    argp.add_argument('--mqtt-username', help='MQTT username')
    argp.add_argument('--mqtt-password', help='MQTT password')
    argp.add_argument('--mqtt-cafile', help='MQTT cafile')
    argp.add_argument('--mqtt-certfile', help='MQTT certfile')
    argp.add_argument('--mqtt-keyfile', help='MQTT keyfile')
    argp.add_argument('-D', '--debug', help='print debug messages', action='store_true')
    argp.add_argument('-v', '--version', action='version', version='%(prog)s ' + __version__)
    args = argp.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO, format=LOG_FORMAT)

    if args.config:
        try:
            with open(args.config, 'r') as f:
                config_yaml = yaml.load(f)
                for key in config.keys():
                    if type(config[key]) == dict:
                        config[key].update(config_yaml.get(key, {}))
                    elif key in config_yaml:
                        config[key] = config_yaml[key]
                if not config['mqtt']['certfile']:
                    config['mqtt']['certfile'] = None
                if not config['mqtt']['keyfile']:
                    config['mqtt']['keyfile'] = None
        except Exception as e:
            logging.error('Failed opening configuration file')
            raise e
            sys.exit(1)

    config['mqtt']['host'] = args.mqtt_host if args.mqtt_host else config['mqtt']['host']
    config['mqtt']['port'] = args.mqtt_port if args.mqtt_port else config['mqtt']['port']
    config['mqtt']['topic'] = args.mqtt_topic if args.mqtt_topic else config['mqtt']['topic']
    config['mqtt']['username'] = args.mqtt_username if args.mqtt_username else config['mqtt']['username']
    config['mqtt']['password'] = args.mqtt_password if args.mqtt_password else config['mqtt']['password']
    config['mqtt']['cafile'] = args.mqtt_cafile if args.mqtt_cafile else config['mqtt']['cafile']
    config['mqtt']['certfile'] = args.mqtt_certfile if args.mqtt_certfile else config['mqtt']['certfile']
    config['mqtt']['keyfile'] = args.mqtt_keyfile if args.mqtt_keyfile else config['mqtt']['keyfile']

    print(config)

    base_topic = config['mqtt']['topic'].rstrip('/') + '/'

    #influx = toinflux.BigClownInflux(config["influx"]["config"],
    #                                 mqtt_rules,
    #                                 config["influx"]["prefix"])

    mqttc = paho.mqtt.client.Client(userdata={'base_topic': base_topic,
                                              'leds': config['leds']})

    mqttc.on_connect = mqtt_on_connect
    mqttc.on_message = mqtt_on_message
    mqttc.username_pw_set(config['mqtt']['username'], config['mqtt']['password'])
    if config['mqtt']['cafile']:
        mqttc.tls_set(config['mqtt']['cafile'], config['mqtt']['certfile'], config['mqtt']['keyfile'])
    mqttc.connect(config['mqtt']['host'], int(config['mqtt']['port']), keepalive=10)
    mqttc.loop_forever()


if __name__ == '__main__':
    main()
